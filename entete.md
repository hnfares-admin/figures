---
title:  "TD 1: Objets et classes"
author: Laurent Pierron (Laurent.Pierron@inria.fr)
affiliation: INRIA / Université de Lorraine
tags: [nothing, nothingness]
documentclass: scrartcl
date: 27 Janvier 2021
papersize: a4paper
geometry: "left=2.5cm,right=2.5cm,top=2cm,bottom=2cm"
output: pdf_document
...
